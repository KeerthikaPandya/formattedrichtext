import { LightningElement } from 'lwc';
export default class MyComponentName extends LightningElement {
    richtext = "<h2>Default <s>Value</s></h2>";

    handleChange(e) {
        this.richtext = e.detail.value;
    }
}